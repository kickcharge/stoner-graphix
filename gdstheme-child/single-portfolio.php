<?php get_header(); ?>

	<?php get_template_part('inc/modules/content', 'title'); ?>
	
	<div class="content-container">
	        
	        <?php if(get_field('breadcrumbs_positioning', 'option') == 'content' && function_exists('yoast_breadcrumb') ) { ?>
	        
		        <div class="row breadcrumb-row">
		          <div class="medium-12 columns">
		            <?php yoast_breadcrumb('<div class="breadcrumbs">','</div>'); ?>
		          </div>
		        </div>
	        
	        <?php } ?>
	        
	  		<div class="row">
		  			
	  			<div class="large-12 columns">
		
				<!-- Gallery wrapper -->
				
					<?php
						$images = get_field('photo_gallery');
						if( $images ):
					?>
					
						<ul class="medium-up-4 small-up-1 gallery" data-equalizer data-clearing data-accessible>
					        <?php foreach( $images as $image ): ?>
					            <li class="grid-item columns" data-equalizer-watch>
					            		
					                	<a href="<?php echo $image['sizes']['large']; ?>" data-fancybox="gallery" data-caption="<?php echo $image['caption']; ?>" class="gallery-thumb thumbnail">
						                	<span class="icon icon-expand"></span>
						                	<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
						                </a>              						
					                    
					            </li>
					        <?php endforeach; ?>
					    </ul>
				    
					<?php endif; ?>
	
			</div><!-- /#content -->
		  
		</div> <!-- /.row -->
		
	</div> <!-- /.content-container -->
	
	<div class="reveal large" id="galleryModal" data-animation-in="fade-in" data-animation-out="fade-out" data-reveal>
		<div class="rev"> 
			<button class="prev-button" type="button">
				<span><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
			</button>
		
			<button class="next-button" type="button">
				<span><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
			</button>
		
			<button class="close-button" data-close aria-label="Close modal" type="button">
				<span>&times;</span>
			</button>
			
			<img class="js-modal-preview modal-preview" src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
		</div>
	</div>

<?php get_footer(); ?>