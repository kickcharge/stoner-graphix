<?php if(is_post_type_archive('portfolio') ) : ?>

	<div class="gallery-items">

		<?php
			$posts = new WP_Query(array(
				'post_type' => 'portfolio',
				'posts_per_page' => -1,
				'orderby' => 'date',
				'order' => 'ASC'
				)
			);
			while ( $posts->have_posts() ) : $posts->the_post();
		?>

		<div class="gallery-item">
			<?php the_post_thumbnail('mini-gallery'); ?>
          	<div class="gallery-item-info">
            	<div class="gallery-item-info-wrap">
              	<div class="caption"><?php the_title(); ?></div> <!-- /.title -->
              	<div class="location"><?php echo $image['caption']; ?></div> <!-- /.location -->
              	<div class="hoverlink">
                  	<a class="fa fa-link" href="<?php the_permalink(); ?>"></a>
              	</div>
            	</div> <!-- /.gallery-item-info-wrap -->
          	</div> <!-- /.gallery-item-info -->
		</div><!-- /.gallery-item -->

		<?php endwhile; wp_reset_postdata(); ?>

	</div><!-- /.gallery-items -->

<?php else : endif; ?>

<?php if (get_sub_field('gallery_type') == 'album_view') { ?>

	<?php
		$field = get_sub_field_object( 'portfolio' );
		$post_objects = get_sub_field('portfolio');
		if( $post_objects ):
	?>

	<section class="idea-gallery section">

	  <?php if (get_sub_field('section_title_portfolio')) { ?>
	    <div class="row title-row">
	      <div class="large-12 columns">
	        <?php the_sub_field('section_title_portfolio'); ?>
	      </div>
	    </div>
	  <?php } ?>

	  <div class="row small-up-2 medium-up-4" data-equalizer>
			<?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
					<?php setup_postdata($post); ?>
	      <div class="column">
	        <a class="thumbnail" href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail('home-gallery'); ?>
					</a>
	      </div>
	    <?php endforeach; ?>
	  </div>

    <div class="row">
      <div class="large-12 columns text-center">
        <a href="<?php echo get_post_type_archive_link( 'portfolio' ); ?>" class="button"><?php the_sub_field('button_content_portfolio'); ?></a>
      </div>
    </div>
	</section>


    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; // end of $post_objects ?>

<?php } else if (get_sub_field('gallery_type') == 'gallery_view') { ?>

	<?php
		$field = get_sub_field_object( 'portfolio' );
		$post_objects = get_sub_field('portfolio');
		if( $post_objects ):
	?>

      <section class="idea-gallery section" data-field="<?php echo $field['key']; ?>">

        <div class="wrap">

			<h1><?php the_sub_field('section_title_portfolio'); ?></h1>

		  	<div class="gallery-container">

			    <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
			        <?php setup_postdata($post); ?>

			          <div class="gallery-items clearfix">
						<?php
							$images = get_field('photo_gallery');
							$max = 6;
							$i = 0;
							if( $images ):
						?>
				        <?php foreach( $images as $image ): $i++; ?>
				        <?php if( $i > $max){ break; } ?>
			          	<div class="gallery-item">
						        <div class="gallery-item-img">
					                 <img src="<?php echo $image['sizes']['mini-gallery']; ?>" alt="<?php echo $image['alt']; ?>" />

						            <?php if(get_sub_field('image_roll-over_titles') == 'general') : ?>

						              	<div class="gallery-item-info">
						                	<div class="gallery-item-info-wrap">
						                  	<div class="caption"><?php the_title(); ?></div> <!-- /.title -->
						                  	<div class="location"><?php echo $image['caption']; ?></div> <!-- /.location -->
						                  	<div class="hoverlink">
							                  	<a title="<?php echo $image['title']; ?>" data-fancybox-group="portfolio" class="fancybox fa fa-arrows-alt" href="<?php echo $image['url']; ?>">&nbsp;</a>
						                  	</div> <!-- /.icon-expand -->
						                	</div> <!-- /.gallery-item-info-wrap -->
						              	</div> <!-- /.gallery-item-info -->

						            <?php elseif(get_sub_field('image_roll-over_titles') == 'image_title') : ?>

						              	<div class="gallery-item-info">
						                	<div class="gallery-item-info-wrap">
						                  	<div class="caption"><?php echo $image['title']; ?></div> <!-- /.title -->
						                  	<div class="location"><?php echo $image['caption']; ?></div> <!-- /.location -->
						                  	<div class="hoverlink">
							                  	<a title="<?php echo $image['title']; ?>" data-fancybox-group="portfolio" class="fancybox fa fa-arrows-alt" href="<?php echo $image['url']; ?>">&nbsp;</a>
						                  	</div> <!-- /.icon-expand -->
						                	</div> <!-- /.gallery-item-info-wrap -->
						              	</div> <!-- /.gallery-item-info -->

						            <?php elseif(get_sub_field('image_roll-over_titles') == 'none') : ?>

						              	<div class="gallery-item-info">
						                	<div class="gallery-item-info-wrap">
						                  	<div class="location"><?php echo $image['caption']; ?></div> <!-- /.location -->
						                  	<div class="hoverlink">
							                  	<a title="<?php echo $image['title']; ?>" data-fancybox-group="portfolio" class="fancybox fa fa-arrows-alt" href="<?php echo $image['url']; ?>">&nbsp;</a>
						                  	</div> <!-- /.icon-expand -->
						                	</div> <!-- /.gallery-item-info-wrap -->
						              	</div> <!-- /.gallery-item-info -->

									<?php else : endif; ?>

							    </div> <!-- /.gallery-item-img -->

			          	</div> <!-- /.gallery-item -->
					    <?php endforeach; ?>
						<?php endif; ?>
			          </div> <!-- /.gallery-items -->

			    <?php endforeach; ?>

		  	</div><!-- /.gallery-container -->

		    <a href="<?php the_permalink(); ?>" class="button"><?php the_sub_field('button_content_portfolio'); ?></a>

        </div> <!-- /.wrap -->
      </section> <!-- /.idea-gallery -->

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; // end of $post_objects ?>

<?php } ?>
