<?php

// Inner Page Builder Customizer (This will allow the user to pull in content blocks that appear on the home page, based on their selection.)

	// check if the flexible content field has rows of data
	if( have_rows('page_content_builder') ):

	     // loop through the rows of data
	    while ( have_rows('page_content_builder') ) : the_row();

	        if( get_row_layout() == 'blog_content' ):

	        	get_template_part('inc/front-page/content', 'blog');

	        elseif( get_row_layout() == 'portfolio' ):

	        	get_template_part('inc/front-page/content', 'portfolio');

	        elseif( get_row_layout() == 'case_studies' ):

	        	get_template_part('inc/front-page/content', 'case_studies');

	        elseif( get_row_layout() == 'open_ended_content' ):

	        	get_template_part('inc/front-page/content', 'open_ended_blank');

	        elseif( get_row_layout() == 'open_ended_content_parallax' ):

	        	get_template_part('inc/front-page/content', 'open_ended_parallax');

	        elseif( get_row_layout() == 'testimonial_slider' ):

	        	get_template_part('inc/front-page/content', 'testimonial_slider');

	        elseif( get_row_layout() == 'columns' ):

	        	get_template_part('inc/front-page/content', 'columns');

	        elseif( get_row_layout() == 'service_icons' ):

	        	get_template_part('inc/front-page/content', 'service_icons');
	        	
	        elseif( get_row_layout() == 'custom_html_field' ):

	        	get_template_part('inc/front-page/content', 'dev_field');
	        	
	        elseif( get_row_layout() == 'php_include' ):

				get_template_part('inc/front-page/content-' . get_sub_field('php_file_name'));        	

	        endif;

	    endwhile;

	else :

	    // no layouts found

	endif; // end content_builder

?>