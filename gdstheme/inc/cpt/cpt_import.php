<?php
// CPT Creator
add_action( 'init', 'cptui_register_my_cpts' );
function cptui_register_my_cpts() {
	$labels = array(
		"name" => "Case Studies",
		"singular_name" => "Case Study",
		"menu_name" => "Case Studies",
		"all_items" => "All Case Studies",
		"add_new" => "Add New",
		"add_new_item" => "Add New Case Study",
		"edit" => "Edit",
		"edit_item" => "Edit Case Study",
		"new_item" => "New Case Study",
		"view" => "View",
		"view_item" => "View Case Study",
		"search_items" => "Search Case Studies",
		"not_found" => "No Case Studies Found",
		);

	$args = array(
		"labels" => $labels,
		"description" => "This section is dedicated to creating case studies within your website.",
		"public" => true,
		"show_ui" => true,
		"has_archive" => true,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "case-studies", "with_front" => true ),
		"query_var" => true,
		"menu_position" => 5,				"supports" => array( "title", "editor", "revisions", "thumbnail" ),			);
	register_post_type( "case-studies", $args );

	$labels = array(
		"name" => "Portfolio",
		"singular_name" => "Portfolio",
		"menu_name" => "Portfolio",
		"all_items" => "All Portfolio",
		"add_new" => "Add New",
		"add_new_item" => "Add New Portfolio",
		"edit" => "Edit",
		"edit_item" => "Edit Portfolio",
		"new_item" => "New Portfolio",
		"view" => "View",
		"view_item" => "View Portfolio",
		"search_items" => "Search Portfolio",
		);

	$args = array(
		"labels" => $labels,
		"description" => "This section is dedicated to managing your portfolio.",
		"public" => true,
		"show_ui" => true,
		"has_archive" => true,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => true,
		"rewrite" => array( "slug" => "portfolio", "with_front" => true ),
		"query_var" => true,
		"menu_position" => 6,				"supports" => array( "title", "revisions", "thumbnail" ),			);
	register_post_type( "portfolio", $args );

// End of cptui_register_my_cpts()
}

// Custom Taxonomies

add_action( 'init', 'cptui_register_my_taxes' );
function cptui_register_my_taxes() {

	$labels = array(
		"name" => "service-provided",
		"label" => "Services Provided",
		"menu_name" => "Services Provided",
		"all_items" => "All Services",
		"edit_item" => "Edit Service",
		"view_item" => "View Service",
		"update_item" => "Update Service Provided",
		"add_new_item" => "Add New Service",
		"new_item_name" => "New Service",
			);

	$args = array(
		"labels" => $labels,
		"hierarchical" => true,
		"label" => "Services Provided",
		"show_ui" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'service-provided', 'with_front' => true ),
		"show_admin_column" => true,
	);
	register_taxonomy( "service-provided", array( "case-studies" ), $args );

// End cptui_register_my_taxes
}

?>