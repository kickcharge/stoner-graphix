<div id="sidebar">

    <aside class="widget menu-widget">
		<?php
			$children = wp_list_pages(
		    	array(
			    	'post_type'=> 'portfolio',
		       		'title_li' => ' ',
		        	'echo'     => false
				)
			);

		if ( $children ) : ?>

			<h3 class="parent">Menu</h3>
		    <ul class="subpages">
		        <?php echo $children ?>
		    </ul><!-- /.subpages -->

		<?php else : ?>

			<h3 class="parent"><?php the_field('navigation_header', 'option'); ?></h3>
			<?php the_field('sidebar_menu', 'option'); ?>

		<?php endif; ?>

    </aside>

</div><!-- /#sidebar -->
