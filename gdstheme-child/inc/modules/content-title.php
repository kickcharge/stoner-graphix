<section
 <?php if ( has_post_thumbnail() && !(is_archive()) && !(is_category()) && !(is_home()) && !(is_singular('post'))&& !(is_single())) { ?>
   class="banner"
   style="background-image: url(<?php echo the_post_thumbnail_url(); ?>)"
 <?php } else { ?>
   class="banner no-bg"
 <?php } ?>
>
  <div class="row">
    <div class="wrap">
      <span class="page-title">
        <?php
          if (is_archive()) {
            $post_type_obj = get_post_type_object( get_post_type() );
            $archive_title = apply_filters('post_type_archive_title', $post_type_obj->labels->menu_name);
            echo $archive_title;
		  } elseif (is_category()) {
            single_cat_title();
          } elseif (is_home()) {
            $query = new WP_Query( array(
              'p' => get_option( 'page_for_posts'),
              'post_type' => 'any'
            ));
            while ($query->have_posts()) {
              $query->the_post();
              the_title();
            };
            wp_reset_postdata();
          } else {
            the_title();
          }
        ?>
      </span>
      <?php if(is_singular('portfolio')){?>
	      <div class="breadcrumbs"><span><span><a href="<?php echo site_url(); ?>">Home</a> / <span><a href="<?php echo site_url(); ?>/portfolios/">Portfolio</a> / <span class="breadcrumb_last" aria-current="page"><?php the_title(); ?></span></span></span></span></div>
      <?php } elseif(get_field('breadcrumbs_positioning', 'option') == 'header' && function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb('<div class="breadcrumbs">','</div>');
      } else {} ?>
    </div><!-- /.wrapper -->
  </div>
</section> <!-- /.banner -->
