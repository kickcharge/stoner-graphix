<?php

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_555383847716c',
	'title' => 'Header Options',
	'fields' => array (
		array (
			'key' => 'field_555383940260f',
			'label' => 'Main Menu Selector',
			'name' => 'main_menu_selector',
			'type' => 'nav_menu',
			'instructions' => 'Please select which menu you would like to appear in the header.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'save_format' => 'id',
			'container' => 'nav',
			'allow_null' => 1,
		),
		array (
			'key' => 'field_555383bb02610',
			'label' => 'Header Message',
			'name' => 'header_message',
			'type' => 'text',
			'instructions' => 'If you would like to show your contact email or phone number please enter it in here.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_577bce8279232',
			'label' => 'Social Icon List',
			'name' => 'social_icon_list',
			'type' => 'nav_menu',
			'instructions' => 'Select the list of social icons from the menu list.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'save_format' => 'menu',
			'container' => 'nav',
			'allow_null' => 1,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-header',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;

 ?>
